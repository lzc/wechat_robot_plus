<script type="text/javascript">
    var acsource = [
<?php
$k = NULL;
foreach ($commands as $v) {
    $k .= '{label:"' . $v->command . '",'
            . 'value:"' . $v->id . '"},';
}
echo rtrim($k, ',');
?>
    ];
    var msgtype_set = null;

    function show_msgtype_set(msgtype) {
        $("#msgtype_" + msgtype).fadeIn("fast").siblings().hide();
    }

    function get_command(pid) {
        var p = $("#parent_command");
        var datas = {
            "do": "getcommand",
            "id": pid
        };
        $.ajax({
            type: "POST",
            async: false,
            url: "<?php echo site_url('admin/ajax'); ?>",
            data: datas,
            dataType: "json",
            success: function(msg) {
//                console.debug(msg);
                if ('success' === msg['status']) {
                    p.html(msg['data']);
                } else {
                    p.html("<?php echo lang('err_command_not_exist'); ?>");
                }
            }
        });
    }

    $(document).ready(function() {
        //初始化
        msgtype_set = $("#select_msgtype").find("option:selected").text();
        show_msgtype_set(msgtype_set);
        //自动完成,获取command
        $("#parent_command_id").autocomplete({source: acsource, delay: 100, minLength: 0})
                .blur(function() {
            var this_val = $(this).val();
            if (this_val.length > 0) {
                get_command(this_val);
            }
        });
        //标签切换
        $("#select_msgtype").change(function() {
            msgtype_set = $(this).find("option:selected").text();
            show_msgtype_set(msgtype_set);
        });
        //插件页切换
        $("#is_with_plugin").change(function() {
            var plugin_set = $("#plugin_set");
            var thisval = $(this).val();
            if (1 === parseInt(thisval)) {
                plugin_set.fadeIn("fast");
            } else {
                plugin_set.hide();
            }
        });
    });
</script>

<div id="win_r">
    <form action="<?php echo site_url('admin/commands_doadd'); ?>" method="post">
        <ul>
            <li>
                <span class="title"><?php echo lang('command'); ?></span>
                <input type="text" name="command" value="" class="input" maxlength="100" />
            </li>
            <li>
                <span class="title"><?php echo lang('parent_command'); ?>Id</span>
                <input type="text" id="parent_command_id" name="parent_command_id" value="" class="input" maxlength="100" />
                <span id="parent_command" class="m_left_10"></span>
            </li>
            <li>
                <span class="title"><?php echo lang('data_regex'); ?></span>
                <input type="text" name="data_regex" value="" class="input" maxlength="100" />
            </li>
            <li>
                <span class="title"><?php echo lang('check_expire'); ?></span>
                <select name="is_expire" class="input">
                    <option value="0"><?php echo lang('no'); ?></option>
                    <option value="1"><?php echo lang('yes'); ?></option>
                </select>
                <span class="m_left_10 notice"><?php echo lang('check_expire_note'); ?></span>
            </li>
            <li>
                <span class="title"><?php echo lang('use_plugin'); ?></span>
                <select id="is_with_plugin" name="is_with_plugin" class="input">
                    <option value="0"><?php echo lang('no'); ?></option>
                    <option value="1"><?php echo lang('yes'); ?></option>
                </select>
                <span class="m_left_10 notice"><?php echo lang('use_plugin_note'); ?></span>
            </li>
            <li id="plugin_set" class="hide">
                <ul>
                    <li>
                        <span class="title"><?php echo lang('plugin_name'); ?></span>
                        <input type="text" name="plugin_name" value="" class="input" maxlength="100" />
                        <span class="m_left_10 notice"><?php echo lang('plugin_name_note'); ?></span>
                    </li>
                    <li>
                        <span class="title"><?php echo lang('plugin_function'); ?></span>
                        <input type="text" name="plugin_function" value="" class="input" maxlength="100" />
                        <span class="m_left_10 notice"><?php echo lang('plugin_function_note'); ?></span>
                    </li>
                </ul>
            </li>
            <li>
                <span class="title"><?php echo lang('reply_msgtype'); ?></span>
                <select id="select_msgtype" name="reply_msgtype_id" class="input">
                    <?php
                    foreach ($msgtype as $v) {
                        $tmp = '<option value="' . $v->id . '">'
                                . $v->type_name
                                . '</option>';
                        echo $tmp;
                    }
                    ?>
                </select>
                <span class="m_left_10 notice"><?php echo lang('reply_msgtype'); ?></span>
            </li>
            <li>
                <div id="msgtype_text" class="hide">
                    <span class="title"><?php echo lang('reply_content'); ?></span>
                    <textarea name="content" class="input_area"></textarea>
                </div>
                <div id="msgtype_news" class="hide">
                    <ul>
                        <li>
                            <span class="title"><?php echo lang('title'); ?></span>
                            <input type="text" name="news_title" value="" class="wide_input" />
                        </li>
                        <li>
                            <span class="title"><?php echo lang('description'); ?></span>
                            <textarea name="news_description" class="input_area"></textarea>
                        </li>
                        <li>
                            <span class="title"><?php echo lang('pic_url'); ?></span>
                            <input type="text" id="pic_url" name="pic_url" value="" class="wide_input" />
                            <div id="pic_uploader" style="display:inline-block;vertical-align:middle"> 
                                <div style="display:inline-block;width:30px;height:30px;overflow:hidden;background:url(<?php echo public_res('images/folder.png'); ?>)">
                                    <input style="opacity:0" name="userfile" type="file" />
                                </div>
                                <input style="margin-left:10px" type="button" value="<?php echo lang('upload'); ?>" onclick="iframe_uploader('pic_uploader', '<?php echo site_url('admin/doupload'); ?>', 'pic_url');" />
                            </div>
                        </li>
                        <li>
                            <span class="title"><?php echo lang('link_url'); ?></span>
                            <input type="text" name="link_url" value="" class="wide_input" />
                        </li>
                    </ul>
                </div>
                <div id="msgtype_music" class="hide">
                    <ul>
                        <li>
                            <span class="title"><?php echo lang('title'); ?></span>
                            <input type="text" name="music_title" value="" class="wide_input" />
                        </li>
                        <li>
                            <span class="title"><?php echo lang('description'); ?></span>
                            <textarea name="music_description" class="input_area"></textarea>
                        </li>
                        <li>
                            <span class="title"><?php echo lang('music_url'); ?></span>
                            <input type="text" id="music_url" name="music_url" value="" class="wide_input" />
                            <div id="music_uploader" style="display:inline-block;vertical-align:middle"> 
                                <div style="display:inline-block;width:30px;height:30px;overflow:hidden;background:url(<?php echo public_res('images/folder.png'); ?>)">
                                    <input style="opacity:0" name="userfile" type="file" />
                                </div>
                                <input style="margin-left:10px" type="button" value="<?php echo lang('upload'); ?>" onclick="iframe_uploader('music_uploader', '<?php echo site_url('admin/doupload'); ?>', 'music_url');" />
                            </div>
                        </li>
                        <li>
                            <span class="title"><?php echo lang('hqmusic_url'); ?></span>
                            <input type="text" id="hqmusic_url" name="hqmusic_url" value="" class="wide_input" />
                            <div id="hqmusic_uploader" style="display:inline-block;vertical-align:middle"> 
                                <div style="display:inline-block;width:30px;height:30px;overflow:hidden;background:url(<?php echo public_res('images/folder.png'); ?>)">
                                    <input style="opacity:0" name="userfile" type="file" />
                                </div>
                                <input style="margin-left:10px" type="button" value="<?php echo lang('upload'); ?>" onclick="iframe_uploader('hqmusic_uploader', '<?php echo site_url('admin/doupload'); ?>', 'hqmusic_url');" />
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="text_c">
                <input type="submit" value="<?php echo lang('add'); ?>" onclick="submit();" />
            </li> 
        </ul>
    </form>
</div>