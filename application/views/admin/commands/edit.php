<script type="text/javascript">
    var acsource = [
<?php
$k = NULL;
foreach ($commands as $v) {
    $k .= '{label:"' . $v->command . '",'
            . 'value:"' . $v->id . '"},';
}
echo rtrim($k, ',');
?>
    ];
    var msgtype_set = null;

    function show_msgtype_set(msgtype) {
        $("#msgtype_" + msgtype).fadeIn("fast").siblings().hide();
    }

    function show_plugin_set(val) {
        var plugin_set = $("#plugin_set");
        if (1 === parseInt(val)) {
            plugin_set.fadeIn("fast");
        } else {
            plugin_set.hide();
        }
    }

    function get_command(pid) {
        var p = $("#parent_command");
        var datas = {
            "do": "getcommand",
            "id": pid
        };
        $.ajax({
            type: "POST",
            async: false,
            url: "<?php echo site_url('admin/ajax'); ?>",
            data: datas,
            dataType: "json",
            success: function(msg) {
//                console.debug(msg);
                if ('success' === msg['status']) {
                    p.html(msg['data']);
                } else {
                    p.html("<?php echo lang('err_command_not_exist'); ?>");
                }
            }
        });
    }

    $(document).ready(function() {
        //初始化
        msgtype_set = $("#select_msgtype").find("option:selected").text();
        show_msgtype_set(msgtype_set);
        show_plugin_set($("#is_with_plugin").val());
        get_command($("#parent_command_id").val());
        //自动完成,获取command
        $("#parent_command_id").autocomplete({source: acsource, delay: 100, minLength: 0})
                .blur(function() {
            var this_val = $(this).val();
            if (this_val.length > 0) {
                get_command(this_val);
            }
        });
        //标签切换
        $("#select_msgtype").change(function() {
            msgtype_set = $(this).find("option:selected").text();
            show_msgtype_set(msgtype_set);
        });
        //插件页切换
        $("#is_with_plugin").change(function() {
            var thisval = $(this).val();
            show_plugin_set(thisval);
        });
    });
</script>

<div id="win_r">
    <form action="<?php echo site_url('admin/commands_doedit'); ?>" method="post">
        <ul>
            <li>
                <input type="hidden" name="id" value="<?php echo $command->id; ?>" />
                <span class="title"><?php echo lang('command'); ?></span>
                <input type="text" name="command" value="<?php echo $command->command; ?>" class="input" maxlength="100" />
            </li>
            <li>
                <span class="title"><?php echo lang('parent_command'); ?>Id</span>
                <input type="text" id="parent_command_id" name="parent_command_id" value="<?php echo $command->p_cmd_id; ?>" class="input" maxlength="100" />
                <span id="parent_command" class="m_left_10"></span>
            </li>
            <li>
                <span class="title"><?php echo lang('data_regex'); ?></span>
                <input type="text" name="data_regex" value="<?php echo $command->data_regex; ?>" class="input" maxlength="100" />
            </li>
            <li>
                <span class="title"><?php echo lang('check_expire'); ?></span>
                <select name="is_expire" class="input">
                    <option value="0"><?php echo lang('no'); ?></option>
                    <?php
                    if ($command->is_expire) {
                        echo '<option value="1" selected="selected">' . lang('yes') . '</option>';
                    } else {
                        echo '<option value="1">' . lang('yes') . '</option>';
                    }
                    ?>
                </select>
                <span class="m_left_10 notice"><?php echo lang('check_expire_note'); ?></span>
            </li>
            <li>
                <span class="title"><?php echo lang('use_plugin'); ?></span>
                <select id="is_with_plugin" name="is_with_plugin" class="input">
                    <option value="0"><?php echo lang('no'); ?></option>
                    <?php
                    if ($command->is_with_plugin) {
                        echo '<option value="1" selected="selected">' . lang('yes') . '</option>';
                    } else {
                        echo '<option value="1">' . lang('yes') . '</option>';
                    }
                    ?>
                </select>
                <span class="m_left_10 notice"><?php echo lang('use_plugin_note'); ?></span>
            </li>
            <li id="plugin_set" class="hide">
                <ul>
                    <li>
                        <span class="title"><?php echo lang('plugin_name'); ?></span>
                        <input type="text" name="plugin_name" value="<?php echo $command->plugin_name; ?>" class="input" maxlength="100" />
                        <span class="m_left_10 notice"><?php echo lang('plugin_name_note'); ?></span>
                    </li>
                    <li>
                        <span class="title"><?php echo lang('plugin_function'); ?></span>
                        <input type="text" name="plugin_function" value="<?php echo $command->plugin_function; ?>" class="input" maxlength="100" />
                        <span class="m_left_10 notice"><?php echo lang('plugin_function_note'); ?></span>
                    </li>
                </ul>
            </li>
            <li>
                <span class="title"><?php echo lang('reply_msgtype'); ?></span>
                <select id="select_msgtype" name="reply_msgtype_id" class="input">
                    <?php
                    foreach ($msgtype as $v) {
                        $selected = '';
                        if ($command->reply_msgtype_id == $v->id) {
                            $selected = 'selected="selected"';
                        }
                        $tmp = '<option value="' . $v->id . '" ' . $selected . '>'
                                . $v->type_name
                                . '</option>';
                        echo $tmp;
                    }
                    ?>
                </select>
                <span class="m_left_10 notice"><?php echo lang('reply_msgtype'); ?></span>
            </li>
            <li>
                <div id="msgtype_text" class="hide">
                    <span class="title"><?php echo lang('reply_content'); ?></span>
                    <textarea name="content" class="input_area"><?php echo $command->content; ?></textarea>
                </div>
                <div id="msgtype_news" class="hide">
                    <ul>
                        <li>
                            <span class="title"><?php echo lang('title'); ?></span>
                            <input type="text" name="news_title" value="<?php echo $command->title; ?>" class="wide_input" />
                        </li>
                        <li>
                            <span class="title"><?php echo lang('description'); ?></span>
                            <textarea name="news_description" class="input_area"><?php echo $command->description; ?></textarea>
                        </li>
                        <li>
                            <span class="title"><?php
                                echo empty($command->pic_url) ? lang('pic_url') : ('<a target="_blank" href="'
                                        . $command->pic_url . '">'
                                        . lang('pic_url') . '</a>');
                                ?></span>
                            <input type="text" id="pic_url" name="pic_url" value="<?php echo $command->pic_url; ?>" class="wide_input" />
                            <div id="pic_uploader" style="display:inline-block;vertical-align:middle"> 
                                <div style="display:inline-block;width:30px;height:30px;overflow:hidden;background:url(<?php echo public_res('images/folder.png'); ?>)">
                                    <input style="opacity:0" name="userfile" type="file" />
                                </div>
                                <input style="margin-left:10px" type="button" value="<?php echo lang('upload'); ?>" onclick="iframe_uploader('pic_uploader', '<?php echo site_url('admin/doupload'); ?>', 'pic_url');" />
                            </div>
                        </li>
                        <li>
                            <span class="title"><?php
                                echo empty($command->link_url) ? lang('link_url') : ('<a target="_blank" href="'
                                        . $command->link_url . '">'
                                        . lang('link_url') . '</a>');
                                ?></span>
                            <input type="text" name="link_url" value="<?php echo $command->link_url; ?>" class="wide_input" />
                        </li>
                    </ul>
                </div>
                <div id="msgtype_music" class="hide">
                    <ul>
                        <li>
                            <span class="title"><?php echo lang('title'); ?></span>
                            <input type="text" name="music_title" value="<?php echo $command->title; ?>" class="wide_input" />
                        </li>
                        <li>
                            <span class="title"><?php echo lang('description'); ?></span>
                            <textarea name="music_description" class="input_area"><?php echo $command->description; ?></textarea>
                        </li>
                        <li>
                            <span class="title"><?php
                                echo empty($command->music_url) ? lang('music_url') : ('<a target="_blank" href="'
                                        . $command->music_url . '">'
                                        . lang('music_url') . '</a>');
                                ?></span>
                            <input type="text" id="music_url" name="music_url" value="<?php echo $command->music_url; ?>" class="wide_input" />
                            <div id="music_uploader" style="display:inline-block;vertical-align:middle"> 
                                <div style="display:inline-block;width:30px;height:30px;overflow:hidden;background:url(<?php echo public_res('images/folder.png'); ?>)">
                                    <input style="opacity:0" name="userfile" type="file" />
                                </div>
                                <input style="margin-left:10px" type="button" value="<?php echo lang('upload'); ?>" onclick="iframe_uploader('music_uploader', '<?php echo site_url('admin/doupload'); ?>', 'music_url');" />
                            </div>
                        </li>
                        <li>
                            <span class="title"><?php
                                echo empty($command->hqmusic_url) ? lang('hqmusic_url') : ('<a target="_blank" href="'
                                        . $command->hqmusic_url . '">'
                                        . lang('hqmusic_url') . '</a>');
                                ?></span>
                            <input type="text" id="hqmusic_url" name="hqmusic_url" value="<?php echo $command->hqmusic_url; ?>" class="wide_input" />
                            <div id="hqmusic_uploader" style="display:inline-block;vertical-align:middle"> 
                                <div style="display:inline-block;width:30px;height:30px;overflow:hidden;background:url(<?php echo public_res('images/folder.png'); ?>)">
                                    <input style="opacity:0" name="userfile" type="file" />
                                </div>
                                <input style="margin-left:10px" type="button" value="<?php echo lang('upload'); ?>" onclick="iframe_uploader('hqmusic_uploader', '<?php echo site_url('admin/doupload'); ?>', 'hqmusic_url');" />
                            </div>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="text_c">
                <input type="submit" value="<?php echo lang('edit'); ?>" onclick="submit();" />
            </li> 
        </ul>
    </form>
</div>