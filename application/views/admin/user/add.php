<div id="win_r">
    <form action="<?php echo site_url('admin/user_doadd'); ?>" method="post">
        <ul>
            <li>
                <span class="title"><?php echo lang('username'); ?></span>
                <input type="text" name="username" class="input" maxlength="20" />
                <span class="m_left_10 notice"><?php echo lang('username_rule_note'); ?></span>
            </li> 
            <li>
                <span class="title"><?php echo lang('password'); ?></span>
                <input type="password" name="password" class="input" maxlength="20" />
                <span class="m_left_10 notice"><?php echo lang('password_rule_note'); ?></span>
            </li> 
            <li>
                <span class="title"><?php echo lang('re_password'); ?></span>
                <input type="password" name="re_password" class="input" maxlength="20" />
            </li> 
            <li>
                <span class="title"><?php echo lang('authority'); ?></span>
                <select name="role" class="input">
                    <option value="0"><?php echo lang('common_user'); ?></option>
                    <option value="1"><?php echo lang('administrator'); ?></option>
                </select>
            </li> 
            <li class="text_c">
                <input type="submit" value="<?php echo lang('add'); ?>" onclick="submit();" />
            </li> 
        </ul>
    </form>
</div>